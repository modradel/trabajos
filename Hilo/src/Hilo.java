import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
public class Hilo implements Runnable {
    private final int nombre;
    private final int duracion;
    public static void main(String[] args) {
        Random aleatorio = new Random(1337);
        for (int i=0; i<6; i++) {
            new Thread(new Hilo(i, aleatorio.nextInt(10000))).start();
        }
    }
     public Hilo(int nombre, int duracion) {
        this.nombre = nombre;
        this.duracion = duracion;
    }
    public void run() {
        System.out.println("Soy el hilo "+this.nombre+" y he iniciado mi ejecuci�n.");
        System.out.println("Soy el hilo "+this.nombre+" y voy a parar mi ejecuci�n "+this.duracion+" ms.");
        try {
            Thread.sleep(this.duracion);
        } catch (InterruptedException ex) {
             Logger.getLogger(Hilo.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Soy el hilo "+this.nombre+" y contin�o mi ejecuci�n.");
        System.out.println("Soy el hilo "+this.nombre+" y he finalizado mi ejecuci�n.");
        }
     }
